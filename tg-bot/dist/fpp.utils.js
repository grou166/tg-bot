"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.showList = void 0;
const showList = todos => `Твой список дел: \n\n${todos.map(todo => todo.id + (todo.isCompleted ? '✅' : '🛑') + ' ' + todo.name + '\n\n').join('')}`;
exports.showList = showList;
//# sourceMappingURL=fpp.utils.js.map