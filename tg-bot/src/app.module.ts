import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'

import { TelegrafModule } from 'nestjs-telegraf'
import { join } from 'path'
import * as LocalSession from 'telegraf-session-local'
import { AppService } from './app.service'
import { AppUpdate } from './app.update'
import { TaskEntity } from './task.entity'


const sessions = new LocalSession({ database: 'sesson_db.json' })

@Module({
	imports: [
		TelegrafModule.forRoot({
			middlewares: [sessions.middleware()],
			token:'5558705445:AAEcTAFNigX-UrzjRPR2bauEvsqIbaxfUME'
		}),
TypeOrmModule.forRoot({
type: 'postgres',
host: 'localhost',
port: 5432,
database: 'todo-app-tg-bot',
username: 'postgres',
password: 'G1964n10',
entities:[join(__dirname, '**' , '*.entity.{ts,js}')],
migrations:[join(__dirname, '**' , '*.entity.{ts,js}')],
synchronize: true
}),
TypeOrmModule.forFeature([TaskEntity])
  ],
  providers: [AppService , AppUpdate]
})
export class AppModule {}
