export const showList = todos => 
    `Твой список дел: \n\n${todos.map(todo => todo.id + (todo.isCompleted ? '✅' : '🛑')  + ' ' + todo.name + '\n\n').join('')}`
   

