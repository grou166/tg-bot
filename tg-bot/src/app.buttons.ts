import { Markup } from "telegraf";

export function actionButtons(){
    return Markup.keyboard(
    [
        Markup.button.callback('Создать дело','create'),
        Markup.button.callback('Список дел','list'),
        Markup.button.callback('Завершить','done'),
        Markup.button.callback('Отредактировать','edit'),
        Markup.button.callback('Удаление ','delete'),
    ],
    {
        columns: 2
    }
    )
}